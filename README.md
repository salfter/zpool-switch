zpool auto-switcher
===================

This is an auto-switcher for zpool (www.zpool.ca) that chooses the most
profitable coin based on the statistics provided by zpool and the hashrates
supported by your GPUs.  It's similar to my MiningPoolHub and NiceHash
auto-switchers, and is set up similarly, except that configuration is
somewhat more modularized.  In the future, the other switchers will change
to a similar configuration so that it will be possible to share most of your
configuration between the different pools.

I started this under nvOC, but have since migrated my mining rig from nvOC
to a standard Gentoo Linux setup.

Usage
=====

./zpool-switch.py conf-file miner-file algo-map estimate_selection [algo]

The switcher takes four parameters in normal operation:

conf-file: general configuration (JSON)

miner-file: miner configuration (JSON)

algo-map: mapping from the algorithm labels zpool uses to the ones used in
miner-file (JSON)

estimate_selection: one of three values to determine switching behavior,
based on the three estimates provided by zpool: estimate_current,
estimate_last24h, actual_last24h.  estimate_current is likely to lead to
more frequent switching than the other two.

The optional fifth parameter lets you manually pick an algorithm for mining;
this is useful for testing and benchmarking.

zpool-switch configures the cards' fan and power settings, starts the miner
in a screen session, and exits.  It should be called periodically (perhaps
every 10-15 minutes); you could do this with a shell script, a cron job, or
whatever other method you prefer.

miners.json
===========

We should ultimately be able to share this with the MiningPoolHub and NiceHash
switchers.  Each available algorithm is described with the following values:

bin: command line to launch miner.  {HOST}, {PORT}, {USERNAME}, and
  {PASSWORD} will be filled in by the switcher.

power_limit: power limit for GPU(s), W

gpu_oc: GPU overclock, MHz (or negative for underclock; 0 for stock speed)

mem_oc: memory overclock, MHz (or negative or zero)

fan: fan speed, % (0 for automatic)

speed: total mining speed for this algorithm, GH/s

power: total power consumption, kW

gpu_oc, mem_oc, and fan may be provided as a single value each to be applied
to all cards or as an array each to set cards individually.

Any additional fields (such as last_benchmark) are ignored.

algo_map.json
=============

Since different pools may use different labels for the same algorithm, we
need to map what the pool uses to what we used in miners.json.  The index is
the pool's label; the value is our label.

When zpool adds a new algorithm, this file is automatically updated with an
empty entry for the new algorithm.

conf.json
=========

The values the zpool switcher cares about are these:

card_type: if set to "nvidia", we can set card clocks, fan speeds, etc. 
  Other values (such as "amd") aren't implemented.

currency: Coinbase only supports "USD" for this value.  If you use a
  different exchange's data, you may have other options available.

pwrcost: your cost of electric power, in currency units per kWh

min_profit: if the rig's profit falls below this value, stop mining

payment_currency: how you want zpool to pay you

payment_addr: where zpool should send payment

The user_name and miner_name values in the example aren't used by the zpool
switcher, but may be used by other switchers.  As with miners.json, we might
be able to share this file with other pool auto-switchers.

